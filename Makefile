
root.key:
				openssl genrsa -out root.key 2048

root.crt:	root.key
				openssl req -x509 -sha256 -nodes -days 366 -newkey rsa:2048 -key root.key -out root.crt

server.key:
				openssl genrsa -out server.key 2048

server.csr:	server.key
				openssl req -new -key server.key -out server.csr

server.crt: root.crt server.csr
				openssl x509 -req -in server.csr -CA root.crt -CAkey root.key -CAcreateserial -out server.crt -days 1

verify:	server.crt
				openssl verify  -verbose -purpose sslserver -CAfile root.crt server.crt

clean:
				rm -f server.* root.*
