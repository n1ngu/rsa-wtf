
# RSA WTF

Basic recipe to generate your own root and chained certificates.

First generate the root certificate running `make root.crt`. You will be prompted for the certificate subject information. It will also create the root private key. Keep it as secure as suitable for the usage you intend.

Elsewhere, generate a server Certificate Signing Request running `make server.csr`. You will be also prompted for the future certificate subject information. Pay attention to the FQDN field since it must match the "Full Qualified Domain Name" of the server where it will be used. It can be a local domain name if that name is used to resolve the server's address.

Make available the CSR file to the environment with the root key and cert. There, sign it by running `make server.crt`, thus generating the server certificate. You can verify everything worked with `make verify`

Remember to install the root certificate as a source of trust in your systems, e.g. like a custom CA in your browser.

A docker-compose is provided for testing. After performing the above steps, run `docker-compose up -d httpd` and visit https://localhost:8443
